import pygame

# initalize pygame
pygame.init ()

# create the screen
screen = pygame.display.set_mode((800,600))

#Title and Icon
pygame.display.set_caption("Python Invaders")
icon = pygame.image.load('ufo.png')
pygame.display.set_icon(icon)

# game loop
running=True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running=False
    # RGB (red, Green, Blue)
    screen.fill((0, 0, 0))
    pygame.display.update()
